# Asset Management

[Snipe-IT](https://snipeitapp.com/) asset management service

[[_TOC_]]

## Description

This repository contains all of the required configuration files to be able to run a Snipe-IT server
which is used to track assets within the hQ home. 

## Asset Tags

Each item within hQ is individually tagged with an ID number and a label which is physically attached.
This label has a QR code and the ID number which both link the item back to the snipe ui.

## CI/CD

The service and configurations are deployed on changes to the `main` branch in this repository.
Pipeline configuration is imported from the [CICD GitLab - Template repository](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/template)
which defines a common ci pipeline.
